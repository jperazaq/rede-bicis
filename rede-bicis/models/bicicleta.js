var Bicicleta = function (id,color,modelo,ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion =ubicacion;
}

Bicicleta.prototype.toString = function (){
    return 'id= '+ this.id + 'color= '+ this.color + 'modelo= '+ this.modelo + 'ubicacion= '+ this.ubicacion;
}


Bicicleta.allBicis = [];

Bicicleta.add = function(abici){
    Bicicleta.allBicis.push(abici);
}

Bicicleta.findByID = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if(aBici)
        return aBici;
        else
            throw new Error(`No Existe la bicicleta con ID ${aBici}`);
    
}

Bicicleta.removeId = function(aBiciId){   


    for (var i=0; i < Bicicleta.allBicis.length; i++){
        if (Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}



var a = new Bicicleta (1, 'rojo', 'urbana', [9.99447,-84.66466]);
var b = new Bicicleta (2, 'azul', 'montana', [9.99669,-84.64925]);

Bicicleta.add(a);
Bicicleta.add(b);



module.exports = Bicicleta;