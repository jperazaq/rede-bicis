var mymap = L.map('mainmap').setView([9.99447, -84.66466], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoianVsaW9wZXJhemEiLCJhIjoiY2tlcnYwcnJ5MW5zajJyc2FveG91N29jNSJ9.kuA88tSclKHpw-a1ZBAg_g'
}).addTo(mymap);

var marker = L.marker([9.99450, -84.66889]).addTo(mymap);